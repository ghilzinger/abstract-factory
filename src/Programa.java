public class Programa {
    private Boton boton;
    private Checkbox checkbox;

    public Programa(FabricaGUI fabrica){
        this.boton = fabrica.crearBoton();
        this.checkbox = fabrica.crearCheckbox();
    }

    public void mensaje(){
        this.boton.mensaje();
        this.checkbox.mensaje();
    } 
}
