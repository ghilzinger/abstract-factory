public class App {
    private static Programa configurarPrograma() {
        FabricaGUI fabrica;
        String osName = System.getProperty("os.name").toLowerCase();
        if (osName.contains("mac")) {
            fabrica = new FabricaMacOS();
        } else {
            fabrica = new FabricaWindows();
        }
        Programa app = new Programa(fabrica);
        return app;
    }
    public static void main(String[] args) throws Exception {
        Programa app = configurarPrograma();
        app.mensaje();
    }
}
