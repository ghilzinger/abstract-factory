public class FabricaMacOS implements FabricaGUI{

    @Override
    public Boton crearBoton() {
        return new BotonMacOS();
    }

    @Override
    public Checkbox crearCheckbox() {
        return new CheckboxMacOS();
    }
    
}
